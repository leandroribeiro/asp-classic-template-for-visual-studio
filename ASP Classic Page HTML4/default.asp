<%	
	'ASP Conventions
	'http://msdn.microsoft.com/en-us/library/ms972100.aspx
	
	'Thread: Best Practices : ASP Coding, SQL Statements, and Database Structuring
	'http://forums.aspfree.com/code-bank-54/best-practices-asp-coding-sql-statements-database-structuring-59050.html
%>

<%@ LANGUAGE = VBScript %>
<%Option Explicit%>

<!--#include file="include/xxxxxx.asp" -->

<%

    'Response.CodePage = 65001
    'Response.CharSet = "ISO-8859-1" 
    
    Dim objAnyThing 'Global variable

    Call Main()

    Sub Main()
		Dim objAnotherThing 'Local variable
		
		Call objAnyThing.BlaBla()
		Call objAnotherThing.BlaBla()
		
        Call OtherMethod()
    End Sub

    Sub OtherMethod()
		Call objAnyThing.BlaBla()
    End Sub

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="Stylesheet" type="text/css" href="style.css" />
</head>
<body>
</body>
<html>