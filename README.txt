Creating Item Templates
http://msdn.microsoft.com/en-us/library/ms247069%28v=vs.100%29.aspx

How to: Share Templates Across a Development Team
http://msdn.microsoft.com/en-us/library/e1x5ayd4%28v=vs.100%29.aspx

How to: Substitute Parameters in a Template
http://msdn.microsoft.com/en-us/library/ms185311%28v=vs.100%29.aspx

How to: Update Existing Templates
http://msdn.microsoft.com/en-us/library/ms185319%28v=vs.100%29.aspx


REFERENCES:
http://blogs.msdn.com/b/mikhailarkhipov/archive/2005/06/26/432852.aspx